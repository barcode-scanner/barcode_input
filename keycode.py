import sys
import logging
sys.path.append('..')
from barcode_lookup import lookup
from product_storage import storage

SHUTDOWN = '9999999999999'
QUIT = 'Quit'
COLRUYT = 'Collect&Go'
AMAZON = 'Amazon.fr'
WUNDERLIST = 'Wunderlist'
CHANGE = 'Change destination'

def main():
	logger = logging.getLogger('errors')
	hdlr = logging.FileHandler('log/errors.log')
	formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
	hdlr.setFormatter(formatter)
	logger.addHandler(hdlr)
	logger.setLevel(logging.WARNING)

	while True:
		try:
			mode = raw_input("For which mode do you want to scan ?")
			try:
				if mode == SHUTDOWN:
					print ('Your raspberry is shutting down...')
					os.system('sudo shutdown -h now')
				elif mode == QUIT:
					print ('Scanning is closing...')
					break
				elif mode == COLRUYT:
					print ('You chose the Collect&Go webshop')
					while True:
						try:
							barcode = raw_input("Waiting for scanning  ---> ")
							try:
								if barcode == SHUTDOWN:
									print ('Your raspberry is shutting down...')
									os.system('sudo shutdown -h now')
								elif barcode == QUIT:
									print ('Scanning is closing...')
									raise Exception									
								elif barcode == CHANGE:
									print "\nThis is the new status of your basket:"
									print "--------------------------------------"
									storage.showColruytBasket()
									break
								else:
									print "Searching the item with barcode %s..." %(barcode)
									response = lookup.searchColruyt(barcode)
									
									itemId = response['data']['searchResults'][0]['list'][0]['id']
									itemBrand = response['data']['searchResults'][0]['list'][0]['brand']
									itemDescription = response['data']['searchResults'][0]['list'][0]['description']
									itemImagePath = response['data']['searchResults'][0]['list'][0]['overviewImage']
									itemPrice = response['data']['searchResults'][0]['list'][0]['price']
									itemWCode = response['data']['searchResults'][0]['list'][0]['defaultUnit']
									
									print "Product [%s] %s - %s : %s euro" % (itemId, itemBrand, itemDescription, itemPrice)
									storage.addProductToColruytBasket(itemId, 1, itemWCode)
									print "This article was added to your basket!"
							except ValueError as e:
								logger.error("Something went wrong: %s" %(e))
								print "Something went wrong: %s" %(e)
							except Exception as ex:
								raise Exception
							except:
								logger.error("Unexpected error:", sys.exc_info()[0])
								print "Unexpected error:", sys.exc_info()[0]
								break
						except (KeyboardInterrupt, SystemExit):
							logger.error("Catched KeyboardInterrupt")
							print "Catched KeyboardInterrupt"
							break
				elif mode == AMAZON:
					print ('You chose the Amazon webshop')
					while True:
						try:
							barcode = raw_input("Waiting for scanning  ---> ")
							try:
								if barcode == SHUTDOWN:
									print ('Your raspberry is shutting down...')
									os.system('sudo shutdown -h now')
								elif barcode == QUIT:
									print ('Scanning is closing...')
									raise Exception
								elif barcode == CHANGE:
									break
								else:
									break
							except ValueError as e:
								logger.error("Something went wrong: %s" %(e))
								print "Something went wrong: %s" %(e)
							except Exception as ex:
								raise Exception
							except:
								logger.error("Unexpected error:", sys.exc_info()[0])
								print "Unexpected error:", sys.exc_info()[0]
								break
						except (KeyboardInterrupt, SystemExit):
							logger.error("Catched KeyboardInterrupt")
							print "Catched KeyboardInterrupt"
							break
				elif mode == WUNDERLIST:
					print ('You chose to add an item to your Wunderlist')
					while True:
						try:
							barcode = raw_input("Waiting for scanning  ---> ")
							try:
								if barcode == SHUTDOWN:
									print ('Your raspberry is shutting down...')
									os.system('sudo shutdown -h now')
								elif barcode == QUIT:
									print ('Scanning is closing...')
									raise Exception
								elif barcode == CHANGE:
									break
								else:
									print "Searching the item with barcode %s..." %(barcode)
									item = lookup.searchEanCode(barcode)
									storage.addProductToShoppingList(item)
									print "This article has been added to your shopping list! \o/ \o/"
							except ValueError as e:
								logger.error("Something went wrong: %s" %(e))
								print "Something went wrong: %s" %(e)
							except Exception as ex:
								raise Exception
							except:
								logger.error("Unexpected error:", sys.exc_info()[0])
								print "Unexpected error:", sys.exc_info()[0]
								break
						except (KeyboardInterrupt, SystemExit):
							logger.error("Catched KeyboardInterrupt")
							print "Catched KeyboardInterrupt"
							break
			except ValueError as e:
				logger.error("Something went wrong: %s" %(e))
				print "Something went wrong: %s" %(e)
			except Exception as ex:
				break
		except (KeyboardInterrupt, SystemExit):
			logger.error("Catched KeyboardInterrupt")
			print "Catched KeyboardInterrupt"
			break
			

if __name__ == '__main__':
    main()